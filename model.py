import numpy as np
import torch
import torch.nn as nn
class PRSNet(nn.Module):
    def __init__(self):
        super(PRSNet,self).__init__()
        self.convs = nn.Sequential(
            nn.Conv3d(1, 4, kernel_size=3, stride=1 ,padding=1),
            nn.MaxPool3d(2),
            nn.LeakyReLU(0.2, True),
            nn.Conv3d(4, 8, kernel_size=3, stride=1 ,padding=1),
            nn.MaxPool3d(2),
            nn.LeakyReLU(0.2, True),
            nn.Conv3d(8, 16, kernel_size=3, stride=1 ,padding=1),
            nn.MaxPool3d(2),
            nn.LeakyReLU(0.2, True),
            nn.Conv3d(16, 32, kernel_size=3, stride=1 ,padding=1),
            nn.MaxPool3d(2),
            nn.LeakyReLU(0.2, True),
            nn.Conv3d(32, 64, kernel_size=3, stride=1 ,padding=1),
            nn.MaxPool3d(2),
            nn.LeakyReLU(0.2, True)
        )

        for i in range(3):
            last_layer = nn.Linear(16, 4)
            init_plane=[0,0,0,0]
            init_plane[i] = 1
            last_layer.weight.data.zero_()
            last_layer.bias.data = torch.Tensor(init_plane)

            layer = nn.Sequential(
                nn.Linear(64, 32),
                nn.LeakyReLU(0.2, True),
                nn.Linear(32, 16),
                nn.LeakyReLU(0.2, True),
                last_layer
            )
            setattr(self, 'plane_layer'+str(i),layer)

        for i in range(3):
            last_layer = nn.Linear(16, 4)
            init_quat=[0,0,0,0]
            init_quat[i+1] = 1
            last_layer.weight.data.zero_()
            last_layer.bias.data = torch.Tensor(init_quat)

            layer = nn.Sequential(
                nn.Linear(64, 32),
                nn.LeakyReLU(0.2, True),
                nn.Linear(32, 16),
                nn.LeakyReLU(0.2, True),
                last_layer
            )
            setattr(self, 'quat_layer'+str(i),layer)

    def forward(self, input):
        a = self.convs(input)
        a=a.reshape(a.size(0),-1)
        quats = []
        planes = []

        for i in range(3):
            plane_layer = getattr(self,'plane_layer'+str(i))
            b = plane_layer(a)
            plane  = b/(1E-12 + torch.norm(b[:,0:3], dim=1, p=2, keepdim=True))
            planes.append(plane)

        for i in range(3):
            quat_layer = getattr(self,'quat_layer'+str(i))
            b = quat_layer(a)
            quat  = b/(1E-12 + torch.norm(b[:,0:4], dim=1, p=2, keepdim=True))
            quats.append(quat)
        
        return planes,quats
