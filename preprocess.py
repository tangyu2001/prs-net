import torch
import numpy as np
import torch.utils.data as data
import math
import random
import os
import time
bad_path=[
'./shapenet/03211117/cc5e011d644edf395eae7316b4dfcbbe/models/model_normalized.surface.binvox',
'./shapenet/02958343/1c86d4441f5f38d552c4c70ef22e33be/models/model_normalized.surface.binvox',
'./shapenet/02958343/1f5a6d3c74f32053b6163196882ac0ca/models/model_normalized.surface.binvox',
'./shapenet/02958343/4b092065568af127d64c207b9313bbaf/models/model_normalized.surface.binvox',
'./shapenet/02958343/ae9b244f9bee122ba35db63c2ad6fc71/models/model_normalized.surface.binvox',
'./shapenet/02958343/f6ed076d16960558e6748b6322a06ee3/models/model_normalized.surface.binvox',
'./shapenet/02958343/7edb40d76dff7455c2ff7551a4114669/models/model_normalized.surface.binvox',
'./shapenet/02958343/876d92ce6a0e4bf399588eee976baae/models/model_normalized.surface.binvox',
'./shapenet/03642806/b3a2081fc19c7bbac241f229aa037cc2/models/model_normalized.surface.binvox',
'./shapenet/03636649/4f3f01e4b17d018ddaaf0f74e020d2dc/models/model_normalized.surface.binvox',
'./shapenet/02958343/95ebb3fd80f885ad676f197a68a5168a/models/model_normalized.surface.binvox'
]
def rand_voxel(translate,scale,data,dims):
    from loss import rot_transform
    idxs = np.argwhere(data != 0)
    idxs = ((idxs+0.5)/dims) * scale + translate
    idxs = torch.tensor(idxs)
    rand_axi = torch.tensor([random.random(),random.random(),random.random()]) - 0.5
    rand_axi = rand_axi / torch.norm(rand_axi,dim=0,p=2)
    rand_angle = random.random() * np.pi
    xyz = np.sin(rand_angle) * rand_axi
    w = torch.tensor(np.cos(rand_angle))
    rand_quat = torch.cat([w.unsqueeze(0),xyz],dim=0)
    trans_idxs = rot_transform(rand_quat.unsqueeze(0),idxs.unsqueeze(0))
    trans_idxs = trans_idxs.reshape(-1,3)
    trans_idxs = trans_idxs.numpy()

    min_idx = np.min(trans_idxs,axis=0)
    max_idx = np.max(trans_idxs,axis=0)
    translate = list(min_idx)
    scale = np.max(max_idx - min_idx)
    new_idxs = (trans_idxs - translate)/scale
    new_idxs = new_idxs * dims
    new_voxel = np.zeros(dims)
    for i,j,k in new_idxs:
        i=min(int(i),dims[0]-1)
        j=min(int(j),dims[1]-1)
        k=min(int(k),dims[2]-1)
        new_voxel[i,j,k] = 1
    return translate,scale,new_voxel,list(rand_quat.numpy().reshape(-1))

def get_closest_points(voxel,scale,translate):
    cp = np.array(range(32**3)).reshape([32,32,32])
    cp = cp * voxel
    vx = voxel.copy()
    d=np.array([-1,0,1])
    while True:
        idxs = np.argwhere(vx == 1)
        if len(idxs) == 0:
            break
        for i,j,k in idxs:
            for x in i + d:
                if x < 0 or x > 31:
                    break
                for y in j + d:
                    if y < 0 or y > 31:
                        break
                    for z in k + d:
                        if z < 0 or z > 31:
                            break
                        if vx[x,y,z] == 0:
                            vx[x,y,z] = 1
                            cp[x,y,z] = cp[i,j,k]
            vx[i,j,k]=2
    cp_x = cp // (32**2)
    cp_y = (cp - cp_x * (32**2)) // 32
    cp_z = cp % 32
    cp = np.stack([cp_x,cp_y,cp_z],axis=3)
    cp = (cp + 0.5) / 32
    return cp * scale + translate

def read_binvox(binvox_path,model,t):
    fp = open(binvox_path,"rb")
    line = fp.readline().strip()
    if not line.startswith(b'#binvox'):
        raise IOError('Not a binvox file')
    dims = list(map(int, fp.readline().strip().split(b' ')[1:]))
    translate = list(map(float, fp.readline().strip().split(b' ')[1:]))
    scale = list(map(float, fp.readline().strip().split(b' ')[1:]))[0]
    fp.readline()
    raw_data = np.frombuffer(fp.read(), dtype=np.uint8)
    fp.close()
    values, counts = raw_data[::2], raw_data[1::2]
    data = np.repeat(values, counts).astype(np.bool)
    data = data.reshape(dims)

    if model == 'train':
        translate,scale,data,rand_quat = rand_voxel(translate,scale,data,dims)
    else:
        rand_quat = np.array([1,0,0,0])
    v1 = np.zeros([dims[0],dims[1],32])
    for i in range(32):
        v1[:,:,i] = np.max(data[:,:,int(i*(dims[2]/32)):int((i+1)*(dims[2]/32))],axis=2)
    v2 = np.zeros([dims[0],32,32])
    for i in range(32):
        v2[:,i,:] = np.max(v1[:,int(i*(dims[1]/32)):int((i+1)*(dims[1]/32)),:],axis=1)
    v3 = np.zeros([32,32,32])
    for i in range(32):
        v3[i,:,:] = np.max(v2[int(i*(dims[0]/32)):int((i+1)*(dims[0]/32)),:,:],axis=0)
    idxs = np.argwhere(data > 0)
    if len(idxs) >= 1000:
        idxs = list(idxs)
        samples = np.array(random.sample(idxs,1000))
    else:
        print("len<1000",len(idxs),binvox_path)
        sample_idxs=list([])
        for i in range(1000):
            sample_idxs.append(random.randint(0,len(idxs)-1))
        samples = idxs[sample_idxs]
    samples = (samples+0.5)/dims
    samples = samples * scale + translate
    cp = get_closest_points(v3,scale,translate)
    save_path = os.path.split(binvox_path)[0]
    save_path = save_path.replace('shapenet','pth')
    os.makedirs(save_path, exist_ok=True)
    save_data = {}
    save_data['voxel'] = torch.FloatTensor(v3).unsqueeze(0)
    save_data['samples'] = torch.FloatTensor(samples)
    save_data['cp'] = torch.FloatTensor(cp)
    save_data['rand_quat'] = rand_quat
    save_data['scale'] = scale
    save_data['translate'] = translate
    save_name = os.path.join(save_path,str(t)+'.pth')
    torch.save(save_data,save_name)
    print(save_name)
    return save_path

def preprocess(root_dir,model):
    save_paths=list([])
    data_split_path = './data_split'
    categories=os.listdir(root_dir)
    category_list = [category + '_' + model + '.txt' for category in categories]
    for i,category in enumerate(category_list):
        category_path = os.path.join(data_split_path,category)
        fp=open(category_path,"r")
        paths=list([])
        for line in fp.readlines():
            id = line.strip()
            path = os.path.join(root_dir,categories[i],id,'models','model_normalized.surface.binvox')
            if os.path.exists(path) and (path not in bad_path):
                paths.append(path)
        if model == 'train':
            times = math.ceil(4000/len(paths))
            for j in range(times):
                save_paths = save_paths + [read_binvox(binvox_path,model,j) for binvox_path in paths]
        else:
            save_paths = save_paths + [read_binvox(binvox_path,model,0) for binvox_path in paths]
        fp.close()
    torch.save(save_paths,'./'+model+'.pth')

if __name__=="__main__":
    preprocess(root_dir='./shapenet', model='train')
    preprocess(root_dir='./shapenet', model='test')