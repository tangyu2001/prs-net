import torch
import numpy as np
import torch.utils.data as data
import math
import random
import os
import time

class MyDataset(data.Dataset):
    def __init__(self,root_dir,model):
        self.paths=torch.load('./'+model+'.pth')

    def __len__(self):
        return len(self.paths)
    
    def __getitem__(self, index):
        data_path=self.paths[index]
        data = torch.load(data_path)
        data['scale'] = torch.tensor(data['scale'])
        data['translate'] = torch.tensor(data['translate'])
        data['path'] = data_path
        return data
