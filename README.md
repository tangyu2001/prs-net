# PRS_Net

#### 介绍
PRS_Net实现


#### 使用说明

1.  用到的python依赖库：Pytorch、Numpy
2.  请确保shapenet文件夹位于本目录下
3.  预处理

    python preprocess.py

4.  训练

    python train.py

5.  测试

    python test.py

6.  测试集的输出对称面与旋转轴保存在test_result对应目录下的plane.txt和axis.txt中

