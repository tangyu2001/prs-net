import torch
from data import MyDataset
from torch.utils.data import DataLoader
from model import PRSNet
from loss import myVal
import os
import numpy as np
device=torch.device("cuda" if torch.cuda.is_available() else "cpu")
device_ids=[0]

test_dataset = MyDataset(root_dir='/home/tangyu/shapenet/', model='test')
test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False, num_workers=4)
test_model = PRSNet()
test_model = test_model.to(device)
test_model = torch.nn.DataParallel(test_model, device_ids=device_ids)
test_model.load_state_dict(torch.load('./shapenet/81.pth'))
test_model.eval()
val = myVal()
val = val.to(device)
with torch.no_grad():
    for batch_idx , test_data in enumerate(test_loader):
        voxel = test_data['voxel'].to(device)
        cp = test_data['cp'].to(device)
        samples = test_data['samples'].to(device)
        scale = test_data['scale'].to(device)
        translate = test_data['translate'].to(device)
        planes, quats = test_model(voxel)
        val_planes,val_axis = val(voxel, samples, cp, planes, quats, scale, translate)
        save_path = test_data['path'][0]
        save_path = save_path.replace('pth','test_result')
        os.makedirs(save_path, exist_ok=True)
        planes_path = os.path.join(save_path,'planes.txt')
        axis_path = os.path.join(save_path,'axis.txt')
        np.savetxt(planes_path,val_planes)
        np.savetxt(axis_path,val_axis)
