import torch
from data import MyDataset
from torch.utils.data import DataLoader
from model import PRSNet
from loss import myLoss
import time

device=torch.device("cuda" if torch.cuda.is_available() else "cpu")
#device_ids=[0,1,2,3]

train_dataset = MyDataset(root_dir='./shapenet', model='train')
train_loader = DataLoader(train_dataset, batch_size=32, shuffle=False, num_workers=4)
train_model = PRSNet()
train_model=train_model.to(device)
#train_model = torch.nn.DataParallel(train_model, device_ids=device_ids)
criterion = myLoss()
criterion = criterion.to(device)
optimizer = torch.optim.Adam(train_model.parameters(), lr=0.01)
train_model.train()
for epoch in range(200):
    for batch_idx , train_data in enumerate(train_loader):
        print(batch_idx,flush=True)
        voxel = train_data['voxel'].to(device)
        cp = train_data['cp'].to(device)
        samples = train_data['samples'].to(device)
        scale = train_data['scale'].to(device)
        translate = train_data['translate'].to(device)
        planes, quats = train_model(voxel)
        losses = criterion(voxel, samples, cp, planes, quats, scale, translate)
        if batch_idx % 1000 == 0:
            print(planes)
            print(quats)
            print(losses)
        reg_loss = (losses['reg_plane'] + losses['reg_quat']).mean()
        sde_loss = (losses['sde_plane'].sum(1) + losses['sde_quat'].sum(1)).mean()
        loss = reg_loss * 25 + sde_loss
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    torch.save(train_model.cpu().state_dict(), './'+str(epoch)+'.pth')
    train_model.to(device)
torch.save(train_model.cpu().state_dict(), './latest.pth')
