import torch
import torch.nn as nn
import random
import numpy as np

device=torch.device("cuda" if torch.cuda.is_available() else "cpu")

def quat_product(q1, q2):
    w1 = q1[:,:,0]
    x1 = q1[:,:,1]
    y1 = q1[:,:,2]
    z1 = q1[:,:,3]
    w2 = q2[:,:,0]
    x2 = q2[:,:,1]
    y2 = q2[:,:,2]
    z2 = q2[:,:,3]
    pw = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
    px = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
    py = w1 * y2 - x1 * z2 + y1 * w2 + z1 * x2
    pz = w1 * z2 + x1 * y2 - y1 * x2 + z1 * w2
    return torch.stack([pw, px, py, pz], dim=2)

def point_to_voxel(points, scale, translate):
    scale = scale.unsqueeze(1).unsqueeze(2).repeat(1,points.shape[1],points.shape[2])
    translate = translate.unsqueeze(1).repeat(1,points.shape[1],1)
    points =32 * (points - translate)/scale
    return torch.clamp(points.long(),min=0,max=31)

def ref_transform(plane,samples):
    plane = plane.unsqueeze(1).repeat(1,samples.shape[1],1)
    n = plane[:,:,0:3]
    d = plane[:,:,3]
    e = torch.sum(samples * n ,dim=2)
    f = e + d
    g = n.pow(2).sum(2) + 1E-12
    h = 2 * torch.div(f,g)
    u = h.unsqueeze(2).repeat(1,1,3)
    return samples - torch.mul(u,n)

def rot_transform(quat,samples):
    quat = quat.unsqueeze(1).repeat(1, samples.shape[1], 1)
    quat_conj = torch.stack([quat[:, :, 0], -1 * quat[:, :, 1], -1 * quat[:, :, 2], -1 * quat[:, :, 3]], dim=2)
    zero_padding = 0 * samples[:,:,0].clone()
    samples = torch.cat([zero_padding.unsqueeze(2),samples],dim=2)
    return quat_product(quat_product(quat,samples),quat_conj)[:, :, 1:4]

def randq(quat):
    rand_angle = random.random() * np.pi
    quat_axis = quat[:,1:4]
    quat_axis = quat_axis/(1E-12 + torch.norm(quat_axis, dim=1, p=2, keepdim=True))
    w = (quat[:,0] * 0 + np.cos(rand_angle)).unsqueeze(1)
    xyz = quat_axis * np.sin(rand_angle)
    return torch.cat([w,xyz],dim=1)

class myVal(nn.Module):
    def __init__(self):
        super(myVal,self).__init__()
        self.sde_dist = SDE.apply

    def forward(self, voxel, samples, cp, planes, quats, scale, translate):
        sde_plane = [self.sde_dist(ref_transform(plane,samples), cp, voxel, scale, translate)[0] for plane in planes]
        sde_quat = [self.sde_dist(rot_transform(randq(quat),samples), cp, voxel, scale, translate)[0] for quat in quats]
        p = list([])
        q = list([])
        for i in range(3):
            if sde_plane[i] < 1 :
                p.append((planes[i][0],sde_plane[i]))
            if sde_quat[i] < 1 :
                a = quats[i][0][1:4]
                a = a / torch.norm(a, dim=0, p=2, keepdim=True)
                q.append((a,sde_quat[i]))
        flag = 1
        while flag == 1:
            flag = 0
            for i in range(len(p)):
                if flag == 1:
                    break
                for j in range(i+1,len(p)):
                    if np.abs((p[i][0][0:3] * p[j][0][0:3]).sum()) > np.cos(np.pi/6):
                        if p[i][1] > p[j][1]:
                            p.pop(i)
                        else:
                            p.pop(j)
                        flag = 1
                        break
        flag = 1
        while flag == 1:
            flag = 0
            for i in range(len(q)):
                if flag == 1:
                    break
                for j in range(i+1,len(q)):
                    if np.abs((q[i][0] * q[j][0]).sum()) > np.cos(np.pi/6):
                        if q[i][1] > q[j][1]:
                            q.pop(i)
                        else:
                            q.pop(j)
                        flag = 1
                        break
        p = np.array([k[0].numpy() for k in p])
        q = np.array([k[0].numpy() for k in q])
        return p,q


class myLoss(nn.Module):
    def __init__(self):
        super(myLoss,self).__init__()
        self.sde_dist = SDE.apply
    
    def forward(self, voxel, samples, cp, planes, quats, scale, translate):
        losses={}
        x = torch.stack([plane[:,0:3] for plane in planes],dim=2)
        y = torch.transpose(x,1,2)
        losses['reg_plane'] = (torch.matmul(x,y) - torch.eye(3).to(device)).pow(2).sum(2).sum(1)
        x = torch.stack([quat[:,1:4] for quat in quats],dim=2)
        y = torch.transpose(x,1,2)
        losses['reg_quat'] = (torch.matmul(x,y) - torch.eye(3).to(device)).pow(2).sum(2).sum(1)
        losses['sde_plane'] = torch.stack([self.sde_dist(ref_transform(plane,samples), cp, voxel, scale, translate) for plane in planes],dim=1)
        losses['sde_quat'] = torch.stack([self.sde_dist(rot_transform(rand(quat),samples), cp, voxel, scale, translate) for quat in quats],dim=1)
        return losses

class SDE(torch.autograd.Function):
    @staticmethod
    def forward(ctx, trans_points, cp, voxel, scale, translate):
        trans_voxel = point_to_voxel(trans_points,scale,translate)
        idx = (trans_voxel * torch.FloatTensor([32**2, 32, 1]).to(device)).sum(2).long()
        mask = 1 - torch.gather(voxel.view(-1,32**3),1,idx)
        mask = mask.unsqueeze(2).repeat(1,1,3)
        idx = idx.unsqueeze(2).repeat(1,1,3)
        cp = cp.reshape(-1,32**3,3)
        closest_points = torch.gather(cp,1,idx)
        dist = trans_points - closest_points
        dist = dist * mask
        ctx.save_for_backward(dist)
        return torch.pow(dist,2).sum(2).sum(1)

    @staticmethod
    def backward(ctx, grad_output):
        dist, = ctx.saved_tensors
        grad_trans_points = 2 * (dist) / (dist.shape[0])
        return grad_trans_points, None, None, None, None
